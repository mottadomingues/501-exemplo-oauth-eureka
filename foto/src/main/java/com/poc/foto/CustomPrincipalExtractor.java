package com.poc.foto;

import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;

public class CustomPrincipalExtractor implements PrincipalExtractor {

  @Override
  public Object extractPrincipal(Map<String, Object> map) {
    Usuario usuario = new Usuario();
    String id = (String) map.get("id");
    usuario.setId(Integer.parseInt(id));
    usuario.setNome((String) map.get("name"));
    
    return usuario;
  }

}
